class SSP {

    constructor() {
        var i = this;
        $(function() {
            i.setzeButtonEvents();
        });
    }

    // Hier passiert die Logik. Probiere dies zu ergänzen
    spielzug(spielersymbol) {

    }

    labelFuerGewinnStatus(status) {
        var ergebnis = "Verloren";
        switch (status) {
            case -1:
                ergebnis = "Verloren";
                break;
            case 0:
                ergebnis = "Unentschieden";
                break;
            case 1:
                ergebnis = "Gewonnen";
                break;
        }
        return ergebnis;
    }



    setzeButtonEvents() {
      $(".symbol").on("click", this.buttonWurdeGeklickt.bind(this));
    }

    buttonWurdeGeklickt(evt) {
      // Symbol des gedrückten Button in kleingeschrieben
      var spielersymbol = $(evt.target).text().toLowerCase();
      this.spielzug(spielersymbol);
    }


    schreibeErgebnis(spielersymbol, gegnersymbol, ergebnis) {
       $(".result").html(`<p>Du: <strong id="spielersymbol">${spielersymbol}</strong> <br/>vs <br/> Gegner: <strong id="gegnersymbol">${gegnersymbol}</strong> <br/><br/> => <strong id="result">${ergebnis}</strong></p>`);
    }


    ermittleGegnerSymbol() {
        var min = 0;
        var max = symbole.length - 1;

        var zahl = Math.round(Math.random() * (max - min)) + min;
        return symbole[zahl];
    }

    // 0 = unentschieden, 1 = gewonnen, -1 = verloren
    ermittleGewinnStatus(spieler, gegner) {
      return symbolmap[spieler][gegner];
    }

    
}






var symbole = ["schere", "stein", "papier"];


var symbolmap = {
    "schere" : {
        "schere" : 0,
        "stein" : -1,
        "papier" : 1
    },
    "stein" : {
        "schere" : 1,
        "stein" : 0,
        "papier" : -1
    },
    "papier" : {
        "schere" : -1,
        "stein" : 1,
        "papier" : 0
    }
};

new SSP();
