function correctnesscheck(selector, condition) {

    this.selector = selector;
    this.condition = condition;
    this.wasShown = false

    this.createFunction = function(selector, condition) {
        return new Function(`var $elem = $("${selector}"); return !!(${condition})`);
    };

    this.showModal = function() {
        if(!I.wasShown) {
            $("#successModal").modal();
            I.wasShown = true;
        }
    };

    this.checkModalShow = function() {
        if ($(I.selector).length > 0) {
            if (condition) {
                try {
                    var func = I.createFunction(I.selector, I.condition);
                    if (func()) {
                        I.showModal();
                    }
                } catch (e) {
                    console.warn("Error: " + e);
                }
            } else {
                I.showModal();
            }
        }
    };

    this.delayedCheck = function() {
        window.setTimeout(function () {
            I.checkModalShow();
        }, 1000);
    };

    this.check = this.checkModalShow;

    var I = this;

    this.delayedCheck();


}
