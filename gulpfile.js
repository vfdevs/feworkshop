let gulp = require("gulp");

//HTML
const hb = require('gulp-hb');
const data = require('gulp-data');
var rename = require("gulp-rename");

//CSS
var sass = require('gulp-sass');
sass.compiler = require('node-sass');

//JS
var ts = require('gulp-typescript');

gulp.task("html", function () {
    return gulp
        .src('./vendors/html/templates/**/*.hbs', {"base" : "./vendors/html/templates/"})
        .pipe(data((file) => {
            return require(file.path.replace('.hbs', '.json'));
        }))
        .pipe(hb().partials('./vendors/html/partials/**/*.hbs') .helpers('.vendors/html/helpers/*.js'))
        .pipe(rename(function (path) {
            path.extname = ".html";
        }))
        .pipe(gulp.dest("./"))
});

// gulp.task("styles", function() {
//    return gulp.src("./src/styles/*.scss")
//        .pipe(sass().on('error', sass.logError))
//        .pipe(gulp.dest('./web/css'));
// });
//
// gulp.task("js", function() {
//     return gulp.src('src/ts/*.ts')
//         .pipe(ts({
//             noImplicitAny: true,
//         }))
//         .pipe(gulp.dest('./web/js'));
// });

gulp.task("default", gulp.series("html"));