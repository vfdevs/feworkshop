# Frontend Fun
## Wie läuft das hier ab?
In dem Projekt befinden sich mehrere Order mit den Namen "level-x", wobei x die Nummer des Level ist.
Die Level beziehen sich dabei auf HTML, CSS und JavasScript. In jedem Level befindet sich entweder eine README.md, in der weitere Informationen zum jeweiligen Level stehen, oder die Informationen stehen direkt in der HTML Datei. Öffne diese einfach mit deinem Editor. Du fragst dich grade bestimmt "Was für ein Editor?". Kommen wir zum nächsten Punkt.
## Was brauche ich um hier mitzumachen?
Wir benötigen ein paar Tools, die kostenlos sind und keine Admin Rechte benötigen.
- Ihr benötigt erst einmal Den Quellcode: https://gitlab.com/vfdevs/feworkshop/-/archive/master/feworkshop-master.zip
- Dann benötigt ihr ein Tool um den ausgepackten Quellcode zu bearbeiten: https://code.visualstudio.com/docs/?dv=win64user
  - Ladet euch in Visual Studio Code noch als Extension (Strg + Shift + X) "open in browser" runter

